# Spring Boot Upload/Download Excel Files with MySQL database example

The Excel file is a spreadsheet file format created by Microsoft for use with Microsoft Excel. 
You can use the file to create, view, edit, analyse data, charts, budgets and more. In this tutorial, 
I will show you how to upload/import Excel file data into MySQL Database using Spring Boot & Apache POI, then export Rest API to return Excel file from database table.

We’re gonna apply the information above later in this tutorial.

### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-upload-excel-files.git`
2. Go inside the folder: `cd spring-boot-upload-excel-files`
3. Run the application: `mvn clean spring-boot:run`
4. Open POSTMAN Collection on this project folder then import it into POSTMAN
5. Run the API

These are APIs to be exported:

|Methods|Urls|Actions|
| ---| --- | --- |
|POST|	/api/excel/upload|	upload an Excel File|
|GET|	/api/excel/tutorials|	get List of items in db table|
|GET|	/api/excel/download|	download db data as Excel file|

* ExcelHelper provides functions to read/write to Excel file.
* Tutorial data model class corresponds to entity and table tutorials.
* TutorialRepository is an interface that extends JpaRepository for persisting data.
* ExcelService uses ExcelHelper and TutorialRepository methods to save Excel data to MySQL, load data to Excel file, or get all Tutorials from MySQL table.
* ExcelController calls ExcelService methods and export Rest APIs: upload Excel file, get data from MySQL database, download Excel File.
* FileUploadExceptionAdvice handles exception when the controller processes file upload.

### Screen shot

Upload Excel File

![Upload Excel File](img/upload.png "Upload Excel File")

Get List Tutorials

![Get List Tutorials](img/list.png "Get List Tutorials")