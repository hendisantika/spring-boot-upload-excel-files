package com.hendisantika.springbootuploadexcelfiles.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-excel-files
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 20.59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessage {
    private String message;
}
