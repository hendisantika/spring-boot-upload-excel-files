package com.hendisantika.springbootuploadexcelfiles.repository;

import com.hendisantika.springbootuploadexcelfiles.model.Tutorial;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-excel-files
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 20.52
 */
public interface TutorialRepository extends JpaRepository<Tutorial, Long> {
}
